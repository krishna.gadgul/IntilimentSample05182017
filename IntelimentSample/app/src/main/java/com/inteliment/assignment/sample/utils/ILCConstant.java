package com.inteliment.assignment.sample.utils;

public class ILCConstant {
    public static final String KEY_LAYOUT_TYPE_SMALL_CAROUSEL = "small_carousel";
    public static final String KEY_LAYOUT_TYPE_BIG_CAROUSEL = "big_carousel";
    public static final String KEY_LAYOUT_TYPE_TEXT_DECORATION = "text_decoration";
    public static final String KEY_LAYOUT_TYPE_TEXT_LAYOUT = "text_layout";
    public static final String KEY_LAYOUT_TYPE_BUTTON_DECORATION = "button_decoration";
    public static final String FRAGMENT_TITLE = "fragment_title";
    public static final String KEY_LATITUDE = "latitude";
    public static final String KEY_LONGITUDE = "longitude";
}
