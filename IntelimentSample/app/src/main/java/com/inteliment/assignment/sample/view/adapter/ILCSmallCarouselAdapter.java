package com.inteliment.assignment.sample.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.inteliment.assignment.sample.R;
import com.inteliment.assignment.sample.interfaces.ILIAdapterInteractionListener;
import com.inteliment.assignment.sample.model.ILCItemDataList;
import com.inteliment.assignment.sample.utils.ILCConstant;
import com.inteliment.assignment.sample.view.widgets.RoboTextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ILCSmallCarouselAdapter extends RecyclerView.Adapter<ILCSmallCarouselAdapter.ViewHolder> {
    private Context mContext;
    private ArrayList<ILCItemDataList> mHorizontalDataList;
    private ILIAdapterInteractionListener mCallback;


    public ILCSmallCarouselAdapter(Context context, ArrayList<ILCItemDataList> horizontalDataList, ILIAdapterInteractionListener callback) {
        mContext = context;
        mHorizontalDataList = horizontalDataList;
        mCallback = callback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.small_carousel_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mMonthName.setText(mHorizontalDataList.get(position).getmName());
    }

    @Override
    public int getItemCount() {
        if (mHorizontalDataList != null && mHorizontalDataList.size() > 0) {
            return mHorizontalDataList.size();
        } else {
            return 0;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.month_name)
        RoboTextView mMonthName;

        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void onClick(View view) {
            if (mCallback != null) {
                mCallback.onListItemClick(ILCConstant.KEY_LAYOUT_TYPE_SMALL_CAROUSEL, mHorizontalDataList.get(getAdapterPosition()).getmName());
            }
        }
    }
}
