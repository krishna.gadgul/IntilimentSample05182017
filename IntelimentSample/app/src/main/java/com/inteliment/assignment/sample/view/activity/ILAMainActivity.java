package com.inteliment.assignment.sample.view.activity;

import android.os.Bundle;

import com.inteliment.assignment.sample.R;
import com.inteliment.assignment.sample.interfaces.FragmentInteractionListener;
import com.inteliment.assignment.sample.utils.ILCFragmentConstants;

public class ILAMainActivity extends ILABaseActivity implements FragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initGatewayScreen();
    }

    private void initGatewayScreen() {
        if (getSupportFragmentManager().findFragmentById(R.id.main_container) == null) {
            setCurrentFragment(null, ILCFragmentConstants.FRAGMENT_GATEWAY_SCREEN, FRAG_ADD, R.id.main_container);
        }
    }

    @Override
    public void setFragment(Bundle bundle, int fragmentType, int transType) {
        setCurrentFragment(bundle, fragmentType, transType, R.id.main_container);
    }
}
