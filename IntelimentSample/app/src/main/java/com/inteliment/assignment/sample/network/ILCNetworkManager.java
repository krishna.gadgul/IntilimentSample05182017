

package com.inteliment.assignment.sample.network;

import com.inteliment.assignment.sample.model.ILCLocationDataModel;

import java.util.ArrayList;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Helper class to handle
 */

public class ILCNetworkManager {

    private ILCRetrofitInterface mRetrofitInterface;

    public ILCNetworkManager(ILCRetrofitInterface RetrofitInterface) {
        mRetrofitInterface = RetrofitInterface;
    }

    public Subscription getNavigationList(ILCResponseListener<ArrayList<ILCLocationDataModel>> callback) {
        return mRetrofitInterface.getLocationData(ILCNetworkUtils.getURL(ILCNetworkUtils.REQ_LOCATION))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends ArrayList<ILCLocationDataModel>>>() {
                    @Override
                    public Observable<? extends ArrayList<ILCLocationDataModel>> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(callback);
    }
}
