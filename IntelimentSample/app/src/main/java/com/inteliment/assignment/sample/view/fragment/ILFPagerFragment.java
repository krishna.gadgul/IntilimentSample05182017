package com.inteliment.assignment.sample.view.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.inteliment.assignment.sample.R;
import com.inteliment.assignment.sample.utils.ILCConstant;
import com.inteliment.assignment.sample.view.widgets.RoboTextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ILFPagerFragment extends Fragment {
    private String mTitle;
    @BindView(R.id.pager_title)
    RoboTextView mPagerTitle;

    public static ILFPagerFragment newInstance(int page, String title) {
        ILFPagerFragment fragmentFirst = new ILFPagerFragment();
        Bundle args = new Bundle();
        args.putString(ILCConstant.FRAGMENT_TITLE, title);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTitle = getArguments().getString(ILCConstant.FRAGMENT_TITLE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.pager_fragment, container, false);
        ButterKnife.bind(this, view);
        mPagerTitle.setText(mTitle);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), (String) mTitle,
                        Toast.LENGTH_SHORT).show();
            }
        });

        return view;
    }

}
