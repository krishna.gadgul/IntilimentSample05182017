package com.inteliment.assignment.sample.model;

import java.util.ArrayList;
public class ILCHomeListItemData {
    private String layoutType;
    private String inline_text;
    private ArrayList<ILCItemDataList> data;

    public String getLayoutType() {
        return layoutType;
    }

    public void setLayoutType(String layoutType) {
        this.layoutType = layoutType;
    }

    public String getInline_text() {
        return inline_text;
    }

    public void setInline_text(String inline_text) {
        this.inline_text = inline_text;
    }

    public ArrayList<ILCItemDataList> getData() {
        return data;
    }

    public void setData(ArrayList<ILCItemDataList> data) {
        this.data = data;
    }
}
