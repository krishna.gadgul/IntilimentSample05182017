package com.inteliment.assignment.sample.view.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.inteliment.assignment.sample.R;
import com.inteliment.assignment.sample.interfaces.ILIAdapterInteractionListener;
import com.inteliment.assignment.sample.model.ILCHomeListItemData;
import com.inteliment.assignment.sample.model.ILCHomeListModel;
import com.inteliment.assignment.sample.model.ILCItemDataList;
import com.inteliment.assignment.sample.utils.ILCConstant;
import com.inteliment.assignment.sample.view.widgets.RoboTextView;
import com.viewpagerindicator.CirclePageIndicator;
import com.viewpagerindicator.TitlePageIndicator;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ILCHomeListAdapter extends RecyclerView.Adapter<ILCHomeListAdapter.BaseViewHolder> {
    private Context mContext;
    private ILCHomeListModel mHomeListData;
    ILIAdapterInteractionListener mCallback;

    public static final int KEY_LAYOUT_SMALL_CAROUSEL = 0;
    public static final int KEY_LAYOUT_BIG_CAROUSEL = 1;
    public static final int KEY_LAYOUT_TEXT_DECORATION = 2;
    public static final int KEY_LAYOUT_TEXT_LAYOUT = 3;
    public static final int KEY_LAYOUT_BUTTON_DECORATION = 4;
    public static final int KEY_LAYOUT_DEFAULT = 5;
    private String mColor;

    public ILCHomeListAdapter(Context context, ILCHomeListModel homeListData, ILIAdapterInteractionListener callback) {
        mContext = context;
        mHomeListData = homeListData;
        mCallback = callback;
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == KEY_LAYOUT_SMALL_CAROUSEL) {
            return new SmallCarouselViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_small_carousel, parent, false));
        } else if (viewType == KEY_LAYOUT_BIG_CAROUSEL) {
            return new BigCarouselViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_big_carousel, parent, false));
        } else if (viewType == KEY_LAYOUT_TEXT_DECORATION) {
            return new TextDecorationViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_text_decoration, parent, false));
        } else if (viewType == KEY_LAYOUT_TEXT_LAYOUT) {
            return new TextLayoutViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_text_layout, parent, false));
        } else if (viewType == KEY_LAYOUT_BUTTON_DECORATION) {
            return new ButtonDecorationViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_button_decoration, parent, false));
        } else {
            return new TextLayoutViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_text_layout, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        if (holder.getItemViewType() == KEY_LAYOUT_SMALL_CAROUSEL) {
            handleSmallCarousel((SmallCarouselViewHolder) holder, position);
        } else if (holder.getItemViewType() == KEY_LAYOUT_BIG_CAROUSEL) {
            handleBigCarouselLayout((BigCarouselViewHolder) holder, position);
        } else if (holder.getItemViewType() == KEY_LAYOUT_TEXT_DECORATION) {
            handleTextDecorationLayout((TextDecorationViewHolder) holder, position);
        } else if (holder.getItemViewType() == KEY_LAYOUT_TEXT_LAYOUT) {
            handleTextLayout((TextLayoutViewHolder) holder, position);
        } else if (holder.getItemViewType() == KEY_LAYOUT_BUTTON_DECORATION) {
            handleButtonDecorationLayout((ButtonDecorationViewHolder) holder, position);
        } else {
            handleTextLayout((TextLayoutViewHolder) holder, position);
        }
    }

    private void handleButtonDecorationLayout(ButtonDecorationViewHolder holder, int position) {
        if (!TextUtils.isEmpty(mColor)) {
            holder.mBtnRootLyt.setBackgroundColor(Color.parseColor(mColor));
        }

        ArrayList<ILCItemDataList> horizontalDataList = mHomeListData.getData().get(position).getData();
        holder.mButtonList.setHasFixedSize(true);
        holder.mButtonList.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        ILCButtonListAdapter ilcButtonListAdapter = new ILCButtonListAdapter(mContext, horizontalDataList, mCallback);
        holder.mButtonList.setAdapter(ilcButtonListAdapter);
    }

    private void handleTextLayout(TextLayoutViewHolder holder, int position) {
        ILCHomeListItemData homeListItemData = mHomeListData.getData().get(position);
        holder.mInlineText.setText(homeListItemData.getInline_text());
    }

    private void handleTextDecorationLayout(TextDecorationViewHolder holder, int position) {

    }

    private void handleBigCarouselLayout(BigCarouselViewHolder holder, int position) {
        ArrayList<ILCItemDataList> bigListData = mHomeListData.getData().get(position).getData();
        FragmentManager manager = ((AppCompatActivity) mContext).getSupportFragmentManager();
        ILCFragmentPagerAdapter adapterViewPager = new ILCFragmentPagerAdapter(manager, bigListData);
        if (holder.mViewPager != null) {
            holder.mViewPager.setAdapter(adapterViewPager);
            holder.mPagerIndicator.setViewPager(holder.mViewPager);
        }
    }

    private void handleSmallCarousel(SmallCarouselViewHolder holder, int position) {
        ArrayList<ILCItemDataList> horizontalDataList = mHomeListData.getData().get(position).getData();
        holder.mSmallCarouselList.setHasFixedSize(true);
        holder.mSmallCarouselList.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        ILCSmallCarouselAdapter smallCarouselAdapter = new ILCSmallCarouselAdapter(mContext, horizontalDataList, mCallback);
        holder.mSmallCarouselList.setAdapter(smallCarouselAdapter);
        //TODO decorator
    }

    @Override
    public int getItemViewType(int position) {
        if (mHomeListData.getData().get(position).getLayoutType().equalsIgnoreCase(ILCConstant.KEY_LAYOUT_TYPE_SMALL_CAROUSEL)) {
            return KEY_LAYOUT_SMALL_CAROUSEL;
        } else if (mHomeListData.getData().get(position).getLayoutType().equalsIgnoreCase(ILCConstant.KEY_LAYOUT_TYPE_BIG_CAROUSEL)) {
            return KEY_LAYOUT_BIG_CAROUSEL;
        } else if (mHomeListData.getData().get(position).getLayoutType().equalsIgnoreCase(ILCConstant.KEY_LAYOUT_TYPE_TEXT_DECORATION)) {
            return KEY_LAYOUT_TEXT_DECORATION;
        } else if (mHomeListData.getData().get(position).getLayoutType().equalsIgnoreCase(ILCConstant.KEY_LAYOUT_TYPE_TEXT_LAYOUT)) {
            return KEY_LAYOUT_TEXT_LAYOUT;
        } else if (mHomeListData.getData().get(position).getLayoutType().equalsIgnoreCase(ILCConstant.KEY_LAYOUT_TYPE_BUTTON_DECORATION)) {
            return KEY_LAYOUT_BUTTON_DECORATION;
        } else {
            return super.getItemViewType(position);
        }
    }

    @Override
    public int getItemCount() {
        if (mHomeListData != null && mHomeListData.getData() != null && mHomeListData.getData().size() > 0) {
            return mHomeListData.getData().size();
        } else {
            return 0;
        }
    }

    public void updateButtonBackground(String data) {
        mColor = data;
        notifyDataSetChanged();
    }

    public class BaseViewHolder extends RecyclerView.ViewHolder {
        public BaseViewHolder(View itemView) {
            super(itemView);
        }
    }

    public class SmallCarouselViewHolder extends BaseViewHolder {
        @BindView(R.id.small_carousel_list)
        RecyclerView mSmallCarouselList;


        public SmallCarouselViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public class BigCarouselViewHolder extends BaseViewHolder {
        @BindView(R.id.pager)
        ViewPager mViewPager;

        @BindView(R.id.pager_indicator)
        CirclePageIndicator mPagerIndicator;

        public BigCarouselViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public class TextDecorationViewHolder extends BaseViewHolder {

        public TextDecorationViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public class TextLayoutViewHolder extends BaseViewHolder {
        @BindView(R.id.inlineText)
        RoboTextView mInlineText;

        public TextLayoutViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public class ButtonDecorationViewHolder extends BaseViewHolder {
        @BindView(R.id.btn_list)
        RecyclerView mButtonList;

        @BindView(R.id.btn_root_lyt)
        RelativeLayout mBtnRootLyt;

        public ButtonDecorationViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
