package com.inteliment.assignment.sample.view.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.inteliment.assignment.sample.R;
import com.inteliment.assignment.sample.interfaces.ILIAdapterInteractionListener;
import com.inteliment.assignment.sample.model.ILCItemDataList;
import com.inteliment.assignment.sample.utils.ILCConstant;
import com.inteliment.assignment.sample.view.widgets.RoboTextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ILCButtonListAdapter extends RecyclerView.Adapter<ILCButtonListAdapter.ViewHolder> {
    private Context mContext;
    ArrayList<ILCItemDataList> mHorizontalDataList;
    ILIAdapterInteractionListener mCallback;

    public ILCButtonListAdapter(Context context, ArrayList<ILCItemDataList> horizontalDataList, ILIAdapterInteractionListener callback) {
        mContext = context;
        mHorizontalDataList = horizontalDataList;
        mCallback = callback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.button_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mBtnDecorator.setText(mHorizontalDataList.get(position).getmName());
        holder.mBtnDecorator.setBackgroundColor(Color.parseColor(mHorizontalDataList.get(position).getColor_code()));

    }

    @Override
    public int getItemCount() {
        if (mHorizontalDataList != null && mHorizontalDataList.size() > 0) {
            return mHorizontalDataList.size();
        } else {
            return 0;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.btn_decorator)
        RoboTextView mBtnDecorator;

        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void onClick(View view) {
            if (mCallback != null) {
                mCallback.onListItemClick(ILCConstant.KEY_LAYOUT_TYPE_BUTTON_DECORATION, mHorizontalDataList.get(getAdapterPosition()).getColor_code());
            }
        }
    }
}
