package com.inteliment.assignment.sample.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.inteliment.assignment.sample.R;
import com.inteliment.assignment.sample.utils.ILCConstant;

public class ILFMapFragment extends Fragment implements OnMapReadyCallback {
    private MapView mapView;
    private GoogleMap map;
    private double mLattitude;
    private double mLongitude;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragent_map, container, false);

        getIntentData();
        // Gets the MapView from the XML layout and creates it
        mapView = (MapView) view.findViewById(R.id.mapview);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this); //this is important
        return view;
    }

    private void getIntentData() {
        Bundle bundle = getArguments();
        if (bundle.containsKey(ILCConstant.KEY_LATITUDE)) {
            mLattitude = bundle.getDouble(ILCConstant.KEY_LATITUDE);
        }

        if (bundle.containsKey(ILCConstant.KEY_LONGITUDE)) {
            mLongitude = bundle.getDouble(ILCConstant.KEY_LONGITUDE);
        }
    }

    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.getUiSettings().setZoomControlsEnabled(true);
        map.addMarker(new MarkerOptions().position(new LatLng(mLattitude, mLongitude)));
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLattitude, mLongitude), 15));
    }
}
