package com.inteliment.assignment.sample.network;

import com.inteliment.assignment.sample.model.ILCLocationDataModel;

import java.util.ArrayList;
import retrofit2.http.GET;
import retrofit2.http.Url;
import rx.Observable;

public interface ILCRetrofitInterface {
    @GET()
    Observable<ArrayList<ILCLocationDataModel>> getLocationData(@Url String url);
}
