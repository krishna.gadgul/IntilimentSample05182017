package com.inteliment.assignment.sample.utils;

import android.support.v4.app.Fragment;

import com.inteliment.assignment.sample.view.fragment.ILFCustomListFragment;
import com.inteliment.assignment.sample.view.fragment.ILFGatewayScreenFragment;
import com.inteliment.assignment.sample.view.fragment.ILFMapFragment;
import com.inteliment.assignment.sample.view.fragment.ILFNavigationListFragment;

public class ILCFragmentUtils {
    public static String getFragmentTag(int type) {
        switch (type) {
            case ILCFragmentConstants.FRAGMENT_GATEWAY_SCREEN:
                return ILFGatewayScreenFragment.class.getName();

            case ILCFragmentConstants.FRAGMENT_CUSTOM_LIST:
                return ILFCustomListFragment.class.getName();

            case ILCFragmentConstants.FRAGMENT_NAVIGATION_LIST:
                return ILFNavigationListFragment.class.getName();

            case ILCFragmentConstants.FRAGMENT_MAP:
                return ILFMapFragment.class.getName();

            default:
                return Fragment.class.getName();
        }
    }
}
