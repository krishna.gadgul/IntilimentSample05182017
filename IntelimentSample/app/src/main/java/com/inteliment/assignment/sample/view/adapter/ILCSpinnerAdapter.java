package com.inteliment.assignment.sample.view.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.inteliment.assignment.sample.R;
import com.inteliment.assignment.sample.model.ILCLocationDataModel;
import com.inteliment.assignment.sample.view.widgets.RoboTextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ILCSpinnerAdapter extends BaseAdapter {
    private Context mContext;
    ArrayList<ILCLocationDataModel> mDataList;

    @BindView(R.id.spinner_item)
    RoboTextView mSpinnerItem;


    public ILCSpinnerAdapter(Context context, ArrayList<ILCLocationDataModel> dataList) {
        mContext = context;
        mDataList = dataList;
    }

    @Override
    public int getCount() {
        if (mDataList != null) {
            return mDataList.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getItem(int position) {
        return mDataList.get(position);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ILCLocationDataModel ilcLocationDataModel = mDataList.get(position);
        LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
        View spinnerView = inflater.inflate(R.layout.spinner_item, viewGroup, false);
        ButterKnife.bind(this, spinnerView);
        mSpinnerItem.setText(ilcLocationDataModel.getName());
        return spinnerView;
    }
}
