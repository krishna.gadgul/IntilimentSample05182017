package com.inteliment.assignment.sample.network;

public class ILCNetworkUtils {

    private static final int API_MAIN = 100;

    private static String getDomailUrl(int type) {

        switch (getApiSource(type)) {
            case API_MAIN:
                return "http://express-it.optusnet.com.au/";

        }

        return "";
    }

    private static final String END_URL_LOCATION = "sample.json";


    public static final int REQ_LOCATION = 100;


    public static String getURL(int type) {

        String path = "";
        switch (type) {

            case REQ_LOCATION:
                path = END_URL_LOCATION;
                break;
        }

        return getDomailUrl(type) + path;
    }


    public static int getApiSource(int type) {
        switch (type) {
            case REQ_LOCATION:
                return API_MAIN;
            default:
                return API_MAIN;
        }
    }
}
