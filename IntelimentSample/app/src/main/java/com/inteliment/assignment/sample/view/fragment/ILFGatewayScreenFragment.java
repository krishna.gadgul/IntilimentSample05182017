package com.inteliment.assignment.sample.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.inteliment.assignment.sample.R;
import com.inteliment.assignment.sample.interfaces.FragmentInteractionListener;
import com.inteliment.assignment.sample.utils.ILCFragmentConstants;
import com.inteliment.assignment.sample.view.widgets.RoboTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class ILFGatewayScreenFragment extends ILFBaseFragment {

    private Unbinder mUnbinder;
    private FragmentInteractionListener fragmentInteractionListener;

    @BindView(R.id.custom_list_txt)
    RoboTextView mCustomList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_gateway_scren, container, false);
        mUnbinder = ButterKnife.bind(this, view);
        return view;
    }


    @OnClick(R.id.custom_list_txt)
    public void onCustomListClick() {
        if (fragmentInteractionListener != null) {
            fragmentInteractionListener.setFragment(null, ILCFragmentConstants.FRAGMENT_CUSTOM_LIST, FRAG_ADD_WITH_STACK);
        }
    }

    @OnClick(R.id.navigation_list_txt)
    public void onNavigationListClick() {
        if (fragmentInteractionListener != null) {
            fragmentInteractionListener.setFragment(null, ILCFragmentConstants.FRAGMENT_NAVIGATION_LIST, FRAG_ADD_WITH_STACK);
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            fragmentInteractionListener = (FragmentInteractionListener) context;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
