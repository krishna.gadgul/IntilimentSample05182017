package com.inteliment.assignment.sample.contract;

import com.inteliment.assignment.sample.interfaces.ILCBasePresenter;
import com.inteliment.assignment.sample.interfaces.ILCBaseView;
import com.inteliment.assignment.sample.model.BaseModel;
import com.inteliment.assignment.sample.model.ILCLocationDataModel;

import java.util.ArrayList;

public interface ILCNavigationContract {
    interface View extends ILCBaseView {
        void init();

        void onSuccess(ArrayList<ILCLocationDataModel> dataList);

    }

    interface presenter extends ILCBasePresenter {
        void getNavigationData();
    }
}
