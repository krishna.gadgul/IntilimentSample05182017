
package com.inteliment.assignment.sample.presenter;

import android.util.Log;

import com.inteliment.assignment.sample.contract.ILCNavigationContract;
import com.inteliment.assignment.sample.model.ILCLocationDataModel;
import com.inteliment.assignment.sample.network.ILCNetworkErrorHandler;
import com.inteliment.assignment.sample.network.ILCNetworkManager;
import com.inteliment.assignment.sample.network.ILCResponseListener;

import java.util.ArrayList;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public class ILCNavigationPresenter implements ILCNavigationContract.presenter {

    private ILCNetworkManager mNetworkWrapper;
    private ILCNavigationContract.View mView;
    private CompositeSubscription subscriptions;

    public ILCNavigationPresenter(ILCNetworkManager networkWrapper, ILCNavigationContract.View view) {
        mNetworkWrapper = networkWrapper;
        mView = view;
        subscriptions = new CompositeSubscription();
    }


    @Override
    public void start() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {
        subscriptions.unsubscribe();
    }

    @Override
    public void destroy() {

    }

    @Override
    public void getNavigationData() {
        mView.showProgress();
        Subscription subscription = mNetworkWrapper.getNavigationList(new ILCResponseListener<ArrayList<ILCLocationDataModel>>() {
            @Override
            public void onSuccess(ArrayList<ILCLocationDataModel> response) {
                mView.hideProgress();
                mView.onSuccess(response);
                Log.d("KR", "response success");
            }

            @Override
            public void onFailure(Throwable error) {
                Log.d("KR", "response error");
            }

            @Override
            public void onError(Throwable e) {
                mView.hideProgress();
                mView.showError(ILCNetworkErrorHandler.getNetworkErrorMessage(e));
            }

        });
        subscriptions.add(subscription);
    }
}
