package com.inteliment.assignment.sample.dagger;


import com.inteliment.assignment.sample.network.ILCNetworkModule;
import com.inteliment.assignment.sample.view.activity.ILABaseActivity;
import com.inteliment.assignment.sample.view.fragment.ILFBaseFragment;

import javax.inject.Singleton;

import dagger.Component;

/**
 * This specifies the contract between the view and the Presenter.
 */
@Singleton
@Component(modules = {ILCAppModule.class, ILCNetworkModule.class})
public interface ILCAppComponent {

    void inject(ILABaseActivity baseActivity);

    void inject(ILFBaseFragment baseFragment);
}
