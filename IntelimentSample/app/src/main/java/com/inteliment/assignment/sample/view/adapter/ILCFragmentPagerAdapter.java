package com.inteliment.assignment.sample.view.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.inteliment.assignment.sample.model.ILCItemDataList;
import com.inteliment.assignment.sample.view.fragment.ILFPagerFragment;

import java.util.ArrayList;

public class ILCFragmentPagerAdapter extends FragmentStatePagerAdapter {
    ArrayList<ILCItemDataList> mBigListData;

    public ILCFragmentPagerAdapter(FragmentManager fm, ArrayList<ILCItemDataList> bigListData) {
        super(fm);
        mBigListData = bigListData;
    }

    @Override
    public Fragment getItem(int position) {
        return ILFPagerFragment.newInstance(0, mBigListData.get(position).getmName());
    }

    @Override
    public int getCount() {
        if (mBigListData != null && mBigListData.size() > 0) {
            return mBigListData.size();
        } else {
            return 0;
        }
    }
}
