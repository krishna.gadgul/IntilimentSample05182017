package com.inteliment.assignment.sample.dagger;


import com.inteliment.assignment.sample.ILCApplication;
import com.inteliment.assignment.sample.interfaces.RxBus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;


@Module
public class ILCAppModule {
    private ILCApplication mILCApplication;

    public ILCAppModule(ILCApplication mILCApplication) {
        this.mILCApplication = mILCApplication;
    }

    @Provides
    @Singleton
    ILCApplication provideApplication() {
        return mILCApplication;
    }

    @Provides
    @Singleton
    public RxBus providesRxBus() {
        return new RxBus();
    }
}