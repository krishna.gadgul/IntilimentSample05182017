package com.inteliment.assignment.sample.utils;

import android.content.Context;

import java.io.IOException;
import java.io.InputStream;

public class ILCFileUtils {

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public static String loadJSONFromAsset(Context context, String fileName) {
        String json = null;
        try {
            InputStream is = null;
            int size;
            try {
                is = context.getResources().getAssets().open(fileName);
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (is != null) {
                size = is.available();
                byte[] buffer = new byte[size];
                is.read(buffer);
                is.close();
                json = new String(buffer, "UTF-8");
            }

        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}
