package com.inteliment.assignment.sample.model;

import java.util.ArrayList;

public class ILCHomeListModel {
    private ArrayList<ILCHomeListItemData> data;

    public ArrayList<ILCHomeListItemData> getData() {
        return data;
    }

    public void setData(ArrayList<ILCHomeListItemData> data) {
        this.data = data;
    }
}
