package com.inteliment.assignment.sample.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.inteliment.assignment.sample.R;
import com.inteliment.assignment.sample.interfaces.ILIAdapterInteractionListener;
import com.inteliment.assignment.sample.model.ILCHomeListModel;
import com.inteliment.assignment.sample.utils.ILCConstant;
import com.inteliment.assignment.sample.utils.ILCFileUtils;
import com.inteliment.assignment.sample.view.adapter.ILCHomeListAdapter;
import com.inteliment.assignment.sample.view.widgets.RoboTextView;
import com.inteliment.assignment.sample.view.widgets.SpaceItemDecorator;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class ILFCustomListFragment extends ILFBaseFragment implements ILIAdapterInteractionListener {
    private Unbinder mUnbinder;

    @BindView(R.id.title_toolbar)
    RoboTextView titleToolbar;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.ilc_list)
    RecyclerView mHomeList;

    private ILCHomeListModel mHomeListModel;

    private ILCHomeListAdapter mHomeListAdapter;


    @OnClick(R.id.btn_back)
    public void backPressed() {
        getActivity().onBackPressed();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_custom_layout, container, false);
        mUnbinder = ButterKnife.bind(this, view);
        initData();
        return view;
    }

    private void initData() {
        try {
            Gson mGson = new Gson();
            mHomeListModel = mGson.fromJson(ILCFileUtils.loadJSONFromAsset(getActivity(), "home_data_list.json"), ILCHomeListModel.class);
            Toast.makeText(getActivity(), "" + mHomeListModel.getData().size(), Toast.LENGTH_SHORT).show();
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolbar();
        initHomeList();
    }

    private void initHomeList() {
        mHomeList.setHasFixedSize(true);
        mHomeList.setLayoutManager(new LinearLayoutManager(getActivity()));
        mHomeListAdapter = new ILCHomeListAdapter(getActivity(), mHomeListModel, this);
        mHomeList.setAdapter(mHomeListAdapter);

        mHomeList.addItemDecoration(new SpaceItemDecorator(20));
        //TODO Decoration is pending
    }

    private void setupToolbar() {
        titleToolbar.setText(getActivity().getResources().getString(R.string.custom_list));
    }

    @Override
    public void onListItemClick(String layoutType, String data) {
        if (layoutType.equalsIgnoreCase(ILCConstant.KEY_LAYOUT_TYPE_BUTTON_DECORATION)) {
            mHomeListAdapter.updateButtonBackground(data);
        } else if (layoutType.equalsIgnoreCase(ILCConstant.KEY_LAYOUT_TYPE_SMALL_CAROUSEL)) {
            mHomeListModel.getData().get(3).setInline_text(data);
            mHomeListAdapter.notifyDataSetChanged();
        }
    }
}
