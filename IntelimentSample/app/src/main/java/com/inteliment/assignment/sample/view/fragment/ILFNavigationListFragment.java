package com.inteliment.assignment.sample.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.inteliment.assignment.sample.R;
import com.inteliment.assignment.sample.contract.ILCNavigationContract;
import com.inteliment.assignment.sample.interfaces.FragmentInteractionListener;
import com.inteliment.assignment.sample.model.ILCLocationDataModel;
import com.inteliment.assignment.sample.presenter.ILCNavigationPresenter;
import com.inteliment.assignment.sample.utils.ILCConstant;
import com.inteliment.assignment.sample.utils.ILCFragmentConstants;
import com.inteliment.assignment.sample.view.adapter.ILCSpinnerAdapter;
import com.inteliment.assignment.sample.view.widgets.RoboTextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import butterknife.Unbinder;

public class ILFNavigationListFragment extends ILFBaseFragment implements ILCNavigationContract.View {

    @BindView(R.id.title_toolbar)
    RoboTextView titleToolbar;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.spinner)
    Spinner mSpinner;

    @BindView(R.id.car)
    RoboTextView mCar;

    @BindView(R.id.train)
    RoboTextView mTrain;

    @BindView(R.id.progress_lyt)
    RelativeLayout mProgressLyt;


    private Unbinder mUnbinder;

    private ILCNavigationPresenter mRegisterUserPresenter;

    ArrayList<ILCLocationDataModel> mDataList;

    private ILCSpinnerAdapter mSpinnerAdapter;
    private ILCLocationDataModel mCurrentItem;
    private FragmentInteractionListener fragmentInteractionListener;


    @OnClick(R.id.btn_back)
    public void backPressed() {
        getActivity().onBackPressed();
    }

    @OnItemSelected(R.id.spinner)
    public void spinnerItemSelected(Spinner spinner, int position) {
        mCurrentItem = mDataList.get(position);
        setCurrentItemData();
    }

    @OnClick(R.id.show_map)
    public void onShowMapClick() {
        if (fragmentInteractionListener != null) {
            Bundle bundle= new Bundle();
            bundle.putDouble(ILCConstant.KEY_LATITUDE,mCurrentItem.getLocation().getLatitude());
            bundle.putDouble(ILCConstant.KEY_LONGITUDE,mCurrentItem.getLocation().getLongitude());
            fragmentInteractionListener.setFragment(bundle, ILCFragmentConstants.FRAGMENT_MAP, FRAG_ADD_WITH_STACK);
        }
    }

    private void setCurrentItemData() {
        try {
            if (mCurrentItem.getFromcentral().getCar() != null) {
                mCar.setVisibility(View.VISIBLE);
                mCar.setText("Car: " + mCurrentItem.getFromcentral().getCar());
            } else {
                mCar.setVisibility(View.GONE);
            }

            if (mCurrentItem.getFromcentral().getTrain() != null) {
                mTrain.setVisibility(View.VISIBLE);
                mTrain.setText("Train: " + mCurrentItem.getFromcentral().getTrain());
            } else {
                mTrain.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_navigation_list, container, false);
        mUnbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolbar();
        init();
    }

    private void setupToolbar() {
        titleToolbar.setText(getActivity().getResources().getString(R.string.navigation_list));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            fragmentInteractionListener = (FragmentInteractionListener) context;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void init() {
        mRegisterUserPresenter = new ILCNavigationPresenter(mRSNetworkManager, this);
        mRegisterUserPresenter.getNavigationData();
        showProgress();
    }

    @Override
    public void onSuccess(ArrayList<ILCLocationDataModel> dataList) {
        mDataList = dataList;
        initSpinnerList();
        hideProgress();
    }

    private void initSpinnerList() {
        mSpinnerAdapter = new ILCSpinnerAdapter(getActivity(), mDataList);
        mSpinner.setAdapter(mSpinnerAdapter);
    }

    @Override
    public void showProgress() {
        mProgressLyt.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        mProgressLyt.setVisibility(View.GONE);
    }

    @Override
    public void showError(int stringResource) {

    }
}
