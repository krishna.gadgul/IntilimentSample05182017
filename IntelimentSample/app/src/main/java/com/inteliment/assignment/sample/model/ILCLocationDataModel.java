package com.inteliment.assignment.sample.model;

import com.google.gson.annotations.Expose;

public class ILCLocationDataModel extends BaseModel {

    @Expose
    private int id;

    @Expose
    private String name;

    @Expose
    private ILCFromCentralModel fromcentral;

    @Expose
    private ILCLocationModel location;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ILCFromCentralModel getFromcentral() {
        return fromcentral;
    }

    public void setFromcentral(ILCFromCentralModel fromcentral) {
        this.fromcentral = fromcentral;
    }

    public ILCLocationModel getLocation() {
        return location;
    }

    public void setLocation(ILCLocationModel location) {
        this.location = location;
    }
}
