package com.inteliment.assignment.sample.interfaces;

public interface ILCBasePresenter {
    void start();

    void resume();

    void pause();

    void stop();

    void destroy();
}
