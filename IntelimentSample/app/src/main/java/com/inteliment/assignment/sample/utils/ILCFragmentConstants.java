package com.inteliment.assignment.sample.utils;

/**
 * Class to handle all fragments constants
 */
public class ILCFragmentConstants {
    public static final int FRAGMENT_GATEWAY_SCREEN = 1;
    public static final int FRAGMENT_CUSTOM_LIST = 2;
    public static final int FRAGMENT_NAVIGATION_LIST = 3;
    public static final int FRAGMENT_MAP = 4;
}
